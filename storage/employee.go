package storage

import (
	"time"

	"gorm.io/gorm"
)

type Employee struct {
	gorm.Model
	User       User `gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
	UserID     *uint
	FirstName  string
	LastName   string
	Joining    time.Time
	Email      string
	Timings    []Timing
	Attendance []Attendance
}
