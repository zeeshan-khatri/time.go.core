package storage

import (
	"time"

	"gorm.io/gorm"
)

type Timing struct {
	gorm.Model
	EmployeeID uint
	Date       time.Time
	TimeIn     time.Time
	TimeOut    time.Time
	Comments   string
}
