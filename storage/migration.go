package storage

// Migrate
func migrate() {

	tables := []interface{}{
		&User{},
		&Employee{},
		&AttendanceType{},
		&Attendance{},
		&Timing{},
	}
	DB.Migrator().DropTable(tables...)
	DB.AutoMigrate(tables...)
}
