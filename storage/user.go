package storage

import (
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	Username  string
	Password  string
	CanLogin  bool
	FirstName string
	LastName  string
}
