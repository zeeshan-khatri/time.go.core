package storage

type AttendanceType struct {
	ID        uint `gorm:"primaryKey;autoIncrement:false"`
	Code      string
	Name      string
	IsDefault bool
}

var FullDay = &AttendanceType{ID: 0, Code: "FD", Name: "Full Day", IsDefault: true}
var Late = &AttendanceType{ID: 1, Code: "LT", Name: "Late", IsDefault: true}
var HalfDay = &AttendanceType{ID: 2, Code: "HD", Name: "Half Day", IsDefault: true}
var CasualLeave = &AttendanceType{ID: 3, Code: "CL", Name: "Casual Leave", IsDefault: true}
var SickLeave = &AttendanceType{ID: 4, Code: "SL", Name: "Sick Leave", IsDefault: true}
var AnnualLeave = &AttendanceType{ID: 5, Code: "AL", Name: "Annual Leave", IsDefault: true}
var PaidLeave = &AttendanceType{ID: 6, Code: "PL", Name: "Paid Leave", IsDefault: true}
var UnpaidLeave = &AttendanceType{ID: 7, Code: "UL", Name: "Unpaid Leave", IsDefault: true}

var DefaultAttendanceType []uint
