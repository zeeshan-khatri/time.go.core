package storage

import (
	"time"

	"gorm.io/gorm"
)

type Attendance struct {
	gorm.Model
	EmployeeID uint
	Date       time.Time
	TypeID     uint
	Type       AttendanceType
	Hours      uint
	Comments   string
}
