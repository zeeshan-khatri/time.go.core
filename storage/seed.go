package storage

import (
	"math/rand"
	"time"
)

var employees []Employee
var attendanceType AttendanceType

var date string = "2020-01-01"
var t time.Time
var err error

const (
	layoutISO = "2006-01-02"
)

func seed() {
	t, err = time.Parse(layoutISO, date)
	seedEmployee()
	seedAttendanceType()
	seedAttendance()
}

func seedEmployee() {
	zeeshan := &Employee{
		Joining:   t,
		FirstName: "Zeeshan",
		LastName:  "Khatri",
		Email:     "zeeshan.khatri@gmail.com",
		User: User{
			Username: "zeeshan.khatri",
			Password: "pakistan",
			CanLogin: false,
		},
	}

	danish := &Employee{
		Joining:   t,
		FirstName: "Danish",
		LastName:  "Khatri",
		Email:     "danish.khatri@gmail.com",
		User: User{
			Username: "danish.khatri",
			Password: "pakistan",
			CanLogin: false,
		},
	}

	shadab := &Employee{
		Joining:   t,
		FirstName: "Shadab",
		LastName:  "Khatri",
		Email:     "shadab.khatri@gmail.com",
	}

	DB.Create(zeeshan)
	DB.Create(danish)
	DB.Create(shadab)

	employees = []Employee{*zeeshan, *danish, *shadab}
}

func seedAttendanceType() {
	DB.Create(FullDay)
	DB.Create(Late)
	DB.Create(HalfDay)
	DB.Create(CasualLeave)
	DB.Create(SickLeave)
	DB.Create(AnnualLeave)
	DB.Create(PaidLeave)
	DB.Create(UnpaidLeave)

	DefaultAttendanceType = []uint{
		FullDay.ID,
		Late.ID,
		HalfDay.ID,
		CasualLeave.ID,
		SickLeave.ID,
		AnnualLeave.ID,
		PaidLeave.ID,
		UnpaidLeave.ID}
}

func seedAttendance() {
	limit := int(time.Since(t).Hours() / 24)
	for i := 0; i < limit; i++ {
		today := t.AddDate(0, 0, i)
		if today.Weekday() != 0 && today.Weekday() != 6 {
			for _, employee := range employees {
				attendance := &Attendance{Date: today, EmployeeID: employee.ID, TypeID: randomType(i)}
				DB.Create(attendance)
			}
		}
	}
}

func randomType(i int) uint {
	id := DefaultAttendanceType[0]
	if i%5 == 0 {
		id = DefaultAttendanceType[rand.Intn(7)]
	}
	return id
}
