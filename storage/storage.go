package storage

import (
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var DB *gorm.DB

// Storage initializer
func Init() {

	dsn := "host=localhost user=timego password=timego dbname=timego port=5432 sslmode=disable TimeZone=Asia/Karachi"
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err.Error())
	}

	DB = db

	migrate()
	seed()

}
