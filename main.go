package main

import (
	"time.go/api"
	"time.go/storage"
)

func main() {

	// Initialize DB
	storage.Init()
	api.Init()
}
