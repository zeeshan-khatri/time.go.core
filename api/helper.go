package api

import (
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

const (
	day   = 1
	month = 2
	year  = 3
)

func fromJSON(c *gin.Context, dto interface{}) error {
	err := c.BindJSON(dto)

	if err != nil {
		c.JSON(http.StatusBadRequest, err.Error())
		return err
	}

	return nil
}

func fromURI(c *gin.Context, params interface{}) error {

	err := c.BindUri(params)

	if err != nil {
		c.JSON(http.StatusBadRequest, err.Error())
		return err
	}

	return nil
}

func atoui(s string) (uint, error) {
	v64, err := strconv.ParseUint(s, 10, 32)
	if err != nil {
		return 0, err
	}

	return uint(v64), nil
}

// func (d DateD) getType() uint {
// 	if d.Day == 0 && d.Month == 0 && d.Year == 0 {
// 		return 0
// 	}

// 	if d.Day == 0 {
// 		if d.Month == 0 {
// 			return year
// 		} else {
// 			return month
// 		}
// 	} else {
// 		return day
// 	}
// }

func (d DateD) getDate() time.Time {

	month := time.Month(d.Month)
	date := time.Date(d.Year, month, d.Day, 0, 0, 0, 0, time.UTC)
	return date
}

func (d DateD) getMonth() (time.Time, time.Time) {

	month := time.Month(d.Month)

	from := time.Date(d.Year, month, 1, 0, 0, 0, 0, time.UTC)
	to := from.AddDate(0, 1, -1)
	return from, to
}

func (d DateD) getYear() (time.Time, time.Time) {
	from := time.Date(d.Year, 1, 1, 0, 0, 0, 0, time.UTC)
	to := from.AddDate(1, 0, -1)
	return from, to
}

func (d DateD) setToday() {
	now := time.Now()
	d.Day = now.Day()
	d.Month = now.Month()
	d.Year = now.Year()
}
