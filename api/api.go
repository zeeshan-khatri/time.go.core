package api

import (
	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
)

func Init() {

	router := gin.Default()
	router.Use(static.Serve("/", static.LocalFile("./_public", true)))

	api := router.Group("/api")
	v1 := api.Group("v1")

	authMiddleware := setJWTMiddleware(router)
	setHandlers(v1, authMiddleware)
	// the jwt middleware

	router.Run(":8080")

}

func setHandlers(r *gin.RouterGroup, m *jwt.GinJWTMiddleware) {

	e := r.Group("employees")
	a := r.Group("attendance")

	e.Use(m.MiddlewareFunc())
	a.Use(m.MiddlewareFunc())

	employeeH(e)
	attendanceH(a)
}
