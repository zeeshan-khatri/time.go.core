package api

import (
	"time"
)

type DateD struct {
	Year  int        `uri:"year"`
	Month time.Month `uri:"month"`
	Day   int        `uri:"day"`
}

type DayAttendanceD struct {
	Day       int                   `json:"id"`
	Employees []EmployeeAttendanceD `json:"employees"`
}

type EmployeeAttendanceD struct {
	EmployeeID uint   `json:"id"`
	TypeID     uint   `json:"type,omitempty"`
	Comments   string `json:"comments,omitempty"`
}

type EmployeeAttendanceCountsD struct {
	EmployeeID uint                     `json:"ID"`
	Months     []MonthAttendanceCountsD `json:"months"`
}

type MonthAttendanceCountsD struct {
	Month uint               `json:"month"`
	Types []AttendanceCountD `json:"types"`
}

type AttendanceCountD struct {
	TypeID uint `json:"id"`
	Count  int  `json:"Count"`
}

type MonthAttendanceD struct {
	Month uint          `json:"month"`
	Days  []AttendanceD `json:"days"`
}

type AttendanceD struct {
	Day      int    `json:"day"`
	TypeID   uint   `json:"type,omitempty"`
	Comments string `json:"comments,omitempty"`
}

type EmployeeD struct {
	ID        uint      `json:"id"`
	FirstName string    `binding:"required"`
	LastName  string    `binding:"required"`
	Joining   time.Time `binding:"required"`
	Email     string    `binding:"email"`
}

type IdD struct {
	ID uint `uri:"id" binding:"required"`
}
