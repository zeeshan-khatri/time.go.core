package api

import (
	"time"
)

type AttendanceP struct {
	EmployeeID uint
	Date       time.Time
	TypeID     uint
	Comments   string
}

type EmployeeAttendanceP struct {
	Date     time.Time
	TypeID   uint
	Comments string
}

type AttendanceSummaryP struct {
	EmployeeID      uint
	FirstDayOfMonth time.Time
	TypeID          uint
	Count           int
}

func (p *AttendanceP) toD() *EmployeeAttendanceD {
	return &EmployeeAttendanceD{
		EmployeeID: p.EmployeeID,
		TypeID:     p.TypeID,
		Comments:   p.Comments,
	}
}

func (p *AttendanceSummaryP) toD() *AttendanceCountD {
	return &AttendanceCountD{
		TypeID: p.TypeID,
		Count:  p.Count,
	}
}

func (p *AttendanceSummaryP) month() uint {
	return uint(p.FirstDayOfMonth.Month())
}

func (p *EmployeeAttendanceP) month() uint {
	return uint(p.Date.Month())
}

func (p *EmployeeAttendanceP) toD() *AttendanceD {
	return &AttendanceD{
		Day:      p.Date.Day(),
		TypeID:   p.TypeID,
		Comments: p.Comments,
	}
}
