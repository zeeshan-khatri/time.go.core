package api

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"time.go/storage"
)

func employeeH(r *gin.RouterGroup) {

	// Get All Employees
	r.GET("/", func(c *gin.Context) {

		results := []EmployeeD{}
		query := storage.DB.Model(&storage.Employee{})
		query.Find(&results)

		c.JSON(http.StatusOK, results)
	})

	// Get Employee
	r.GET("/:id", func(c *gin.Context) {
		var e IdD
		erre := fromURI(c, &e)

		if erre == nil {
			result := EmployeeD{}
			query := storage.DB.Model(&storage.Employee{})

			if err := query.First(&result, e.ID).Error; err != nil {
				c.String(http.StatusNotFound, fmt.Sprintf("Employee not found against id: %v", e.ID))
				return
			}

			c.JSON(http.StatusOK, result)
		}

	})

	// Add Employee
	r.POST("/", func(c *gin.Context) {
		var d EmployeeD
		err := fromJSON(c, &d)

		if err == nil {
			employee := storage.Employee{
				FirstName: d.FirstName,
				LastName:  d.LastName,
				Joining:   d.Joining,
				Email:     d.Email,
			}

			storage.DB.Create(&employee)
			c.JSON(http.StatusCreated, gin.H{"id": employee.ID})
		}
	})

	// Update Employees
	r.PUT("/:id", func(c *gin.Context) {
		var e EmployeeD
		err := fromJSON(c, &e)

		if err == nil {
			id := c.Param("id")
			employee := storage.Employee{}
			storage.DB.First(&employee, id)

			employee.FirstName = e.FirstName
			employee.LastName = e.LastName
			employee.Joining = e.Joining
			employee.Email = e.Email

			storage.DB.Save(&employee)

			c.JSON(http.StatusOK, gin.H{"id": employee.ID})
		}
	})

	// Delete Employee
	r.DELETE("/:id", func(c *gin.Context) {
		id := c.Param("id")
		storage.DB.Delete(&storage.Employee{}, id)

		c.JSON(http.StatusOK, gin.H{"id": id})
	})

	// Mark Employee Attendance
	r.PATCH("/:id/attendance/:year/:month/:day", func(c *gin.Context) {
		var e IdD
		var d DateD
		var a EmployeeAttendanceD

		erre := fromURI(c, &e)
		errd := fromURI(c, &d)
		erra := fromJSON(c, &a)

		if erre == nil && errd == nil && erra == nil {
			var attendance storage.Attendance
			storage.DB.Where(&storage.Attendance{EmployeeID: e.ID, Date: d.getDate()}).FirstOrInit(&attendance)
			attendance.TypeID = storage.FullDay.ID
			if a.TypeID > 0 {
				attendance.TypeID = a.TypeID
			}

			storage.DB.Save(&attendance)
			c.JSON(http.StatusCreated, gin.H{"id": attendance.ID})
		}
	})

	r.GET("/:id/attendance/:year", func(c *gin.Context) {
		var e IdD
		var d DateD

		erre := fromURI(c, &e)
		errd := fromURI(c, &d)

		if erre == nil && errd == nil {
			from, to := d.getYear()
			results := yearlyE(e.ID, from, to)
			c.JSON(http.StatusOK, results)
		}
	})

}

func yearlyE(employeeID uint, from time.Time, to time.Time) interface{} {
	results := []EmployeeAttendanceP{}

	query := storage.DB.Model(&storage.Attendance{})
	query.Where("employee_id = ? AND date BETWEEN ? AND ?", employeeID, from, to).Order("date").Find(&results)

	var days []AttendanceD
	monthly := []MonthAttendanceD{}
	var lastMonth uint
	for _, each := range results {

		month := each.month()
		if lastMonth != month {
			if lastMonth != 0 {
				monthly = append(monthly, MonthAttendanceD{Month: lastMonth, Days: days})
			}
			days = []AttendanceD{}
		}

		days = append(days, *each.toD())
		lastMonth = month
	}

	if lastMonth != 0 {
		monthly = append(monthly, MonthAttendanceD{Month: lastMonth, Days: days})
	}

	return monthly
}
