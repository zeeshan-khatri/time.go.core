package api

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"time.go/storage"
)

// Get Daily Attendance
func attendanceH(r *gin.RouterGroup) {
	// Get Employee Attendance
	r.GET(":year/:month/:day", func(c *gin.Context) {
		var d DateD

		errd := fromURI(c, &d)

		if errd == nil {
			results := daily(d.getDate())
			c.JSON(http.StatusOK, results)
		}
	})

	r.GET(":year/:month", func(c *gin.Context) {
		var d DateD

		errd := fromURI(c, &d)

		if errd == nil {
			from, to := d.getMonth()
			results := monthly(from, to)
			c.JSON(http.StatusOK, results)
		}
	})

	r.GET(":year", func(c *gin.Context) {
		var d DateD

		errd := fromURI(c, &d)

		if errd == nil {
			from, to := d.getYear()
			results := yearly(from, to)
			c.JSON(http.StatusOK, results)
		}
	})
}

func daily(date time.Time) []EmployeeAttendanceD {
	results := []EmployeeAttendanceD{}

	query := storage.DB.Model(&storage.Attendance{})
	query.Where(&storage.Attendance{Date: date}).Find(&results)

	return results
}

func monthly(from time.Time, to time.Time) []DayAttendanceD {
	results := []AttendanceP{}

	query := storage.DB.Model(&storage.Attendance{})
	query.Where("date BETWEEN ? AND ?", from, to).Order("date").Find(&results)

	var daily []EmployeeAttendanceD
	monthly := []DayAttendanceD{}
	var defaultTime time.Time
	var last time.Time
	for _, each := range results {

		if last != each.Date {
			if last != defaultTime {
				monthly = append(monthly, DayAttendanceD{Day: last.Day(), Employees: daily})
			}
			daily = []EmployeeAttendanceD{}
		}

		daily = append(daily, *each.toD())
		last = each.Date
	}

	if last != defaultTime {
		monthly = append(monthly, DayAttendanceD{Day: last.Day(), Employees: daily})
	}

	return monthly
}

func yearly(from time.Time, to time.Time) interface{} {
	results := []AttendanceSummaryP{}

	query := storage.DB.Debug().Model(&storage.Attendance{})
	query = query.Select("employee_id, DATE_TRUNC('month',date) AS first_day_of_month, type_id, COUNT(id) as count")
	query = query.Where("date BETWEEN ? AND ?", from, to).Order("employee_id, first_day_of_month, type_id")
	query.Group("employee_id, DATE_TRUNC('month',date), type_id").Find(&results)

	employee := []EmployeeAttendanceCountsD{}
	month := []MonthAttendanceCountsD{}
	counts := []AttendanceCountD{}
	var lastEmployee uint
	var lastMonth uint
	for _, each := range results {

		if lastEmployee != each.EmployeeID {
			if lastEmployee != 0 {
				employee = append(employee, EmployeeAttendanceCountsD{EmployeeID: lastEmployee, Months: month})
			}
			month = []MonthAttendanceCountsD{}
			lastMonth = 0
		}

		if lastMonth != each.month() {
			if lastMonth != 0 {
				month = append(month, MonthAttendanceCountsD{Month: lastMonth, Types: counts})
			}
			counts = []AttendanceCountD{}
		}

		counts = append(counts, *each.toD())
		lastEmployee = each.EmployeeID
		lastMonth = each.month()
	}

	if lastMonth != 0 {
		month = append(month, MonthAttendanceCountsD{Month: lastMonth, Types: counts})
	}

	if lastEmployee != 0 {
		employee = append(employee, EmployeeAttendanceCountsD{EmployeeID: lastEmployee, Months: month})
	}

	return employee
}
